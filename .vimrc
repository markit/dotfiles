call plug#begin()
Plug 'tpope/vim-sensible'
Plug 'altercation/vim-colors-solarized'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'plasticboy/vim-markdown'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'ycm-core/youcompleteme'
Plug 'scrooloose/nerdtree'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'tpope/vim-fugitive'
call plug#end()

syntax enable
set background=dark
colorscheme solarized

set ignorecase
set smartcase

set autoindent

set hlsearch

set number

set shiftwidth=4
set softtabstop=4
set expandtab

set foldmethod=syntax
au BufRead * normal zR

map <C-o> :NERDTreeToggle<CR>
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf

set guifont=Hack\ Regular\ 9

let g:indent_guides_enable_on_vim_startup = 1

set colorcolumn=100,120

set nowrap
